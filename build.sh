#!/bin/sh

cargo clean

cargo +nightly rustc -Zbuild-std=core --target x86_64-unknown-linux-gnu --release --lib -- -C link-arg=-nostartfiles

cargo +nightly rustc -Zbuild-std=core --target x86_64-unknown-linux-gnu --release --bin min_sized -- -C link-arg=-nostartfiles

ls target/x86_64-unknown-linux-gnu/release/deps/ -lh
